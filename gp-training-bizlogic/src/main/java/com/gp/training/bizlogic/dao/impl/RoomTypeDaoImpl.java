package com.gp.training.bizlogic.dao.impl;

import com.gp.training.bizlogic.dao.RoomTypeDao;
import com.gp.training.bizlogic.domain.RoomType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class RoomTypeDaoImpl implements RoomTypeDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public RoomType takeRoomType(Long id) {
		RoomType roomType = entityManager.find(RoomType.class, id);
		return roomType;
	}

	@Override
	public void addRoomType(RoomType roomType) {
		entityManager.persist(roomType);
	}

	@Override
	public void deleteRoomType(Long id) {
		RoomType roomType = entityManager.find(RoomType.class, id);
		if (null != roomType) {
			entityManager.remove(roomType);
		}
	}

	@Override
	public List<RoomType> takeRoomTypes() {
		return entityManager.createQuery("from RoomType", RoomType.class)
				.getResultList();
	}

}
