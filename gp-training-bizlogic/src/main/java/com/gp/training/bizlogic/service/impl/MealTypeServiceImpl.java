package com.gp.training.bizlogic.service.impl;

import com.gp.training.bizlogic.dao.MealTypeDao;
import com.gp.training.bizlogic.domain.MealType;
import com.gp.training.bizlogic.service.MealTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MealTypeServiceImpl implements MealTypeService {
	@Autowired
	MealTypeDao mealTypeDao;

	@Override
	public MealType takeMealType(Long id) {
		return mealTypeDao.takeMealType(id);
	}

	@Override
	public void addMealType(MealType mealType) {
		mealTypeDao.addMealType(mealType);
	}

	@Override
	public void deleteMealType(Long id) {
		mealTypeDao.deleteMealType(id);
	}

	@Override
	public List<MealType> takeMealTypes() {
		return mealTypeDao.takeMealTypes();
	}

}
