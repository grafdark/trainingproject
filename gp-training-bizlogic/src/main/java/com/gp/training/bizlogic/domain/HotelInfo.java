package com.gp.training.bizlogic.domain;

import javax.persistence.*;

@Entity
@Table(name = "hotel_info")
public class HotelInfo {

	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String name;
	@Column
	private String code;
	@Column
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
