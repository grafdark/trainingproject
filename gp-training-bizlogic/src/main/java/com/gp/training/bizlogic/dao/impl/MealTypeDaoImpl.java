package com.gp.training.bizlogic.dao.impl;

import com.gp.training.bizlogic.dao.MealTypeDao;
import com.gp.training.bizlogic.domain.MealType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class MealTypeDaoImpl implements MealTypeDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public MealType takeMealType(Long id) {
		MealType mealType = entityManager.find(MealType.class, id);
		return mealType;
	}

	@Override
	public void addMealType(MealType mealType) {
		entityManager.persist(mealType);
	}

	@Override
	public void deleteMealType(Long id) {
		MealType mealType = entityManager.find(MealType.class, id);
		if (null != mealType) {
			entityManager.remove(mealType);
		}
	}

	@Override
	public List<MealType> takeMealTypes() {
		return entityManager.createQuery("from MealType", MealType.class)
				.getResultList();
	}

}
