package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.RoomType;

import java.util.List;

public interface RoomTypeDao {
	RoomType takeRoomType(Long id);

	void addRoomType(RoomType roomType);

	void deleteRoomType(Long id);

	List<RoomType> takeRoomTypes();
}
