package com.gp.training.bizlogic.service;

import com.gp.training.bizlogic.domain.RoomType;

import java.util.List;

public interface RoomTypeService {
	RoomType takeRoomType(Long id);

	void addRoomType(RoomType roomType);

	void deleteRoomType(Long id);

	List<RoomType> takeRoomTypes();
}
