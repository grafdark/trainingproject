package com.gp.training.bizlogic.dao.impl;

import com.gp.training.bizlogic.dao.CityDao;
import com.gp.training.bizlogic.domain.City;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

@Service
public class CityDaoImpl extends GenericDaoImpl<City, Long> implements CityDao {

	public CityDaoImpl() {
		super(City.class);
	}

	@PersistenceContext	private EntityManager entityManager;

//	@Override
//	public City takeCityInfo(Long id) {
//		City city = entityManager.find(City.class, id);
//		return city;
//	}
//
//	@Override
//	public void addCity(City city) {
//		entityManager.persist(city);
//	}
//
//	@Override
//	public void deleteCity(Long id) {
//		City city = entityManager.find(City.class, id);
//		if (null != city) {
//			entityManager.remove(city);
//		}
//	}
//
//	@Override
//	public List<City> takeCities() {
//		return entityManager.createQuery("from City", City.class)
//				.getResultList();
//	}


}
