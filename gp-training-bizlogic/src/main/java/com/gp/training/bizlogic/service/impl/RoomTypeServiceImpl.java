package com.gp.training.bizlogic.service.impl;

import com.gp.training.bizlogic.dao.RoomTypeDao;
import com.gp.training.bizlogic.domain.RoomType;
import com.gp.training.bizlogic.service.RoomTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomTypeServiceImpl implements RoomTypeService {

	@Autowired
	RoomTypeDao roomTypeDao;

	@Override
	public RoomType takeRoomType(Long id) {
		return roomTypeDao.takeRoomType(id);
	}

	@Override
	public void addRoomType(RoomType roomType) {
		roomTypeDao.addRoomType(roomType);
	}

	@Override
	public void deleteRoomType(Long id) {
		roomTypeDao.deleteRoomType(id);
	}

	@Override
	public List<RoomType> takeRoomTypes() {
		return roomTypeDao.takeRoomTypes();
	}

}
