package com.gp.training.bizlogic.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "country")
public class Country extends AbstractEntity {
	@Column
	private String name;
	@Column
	private String code;
	@OneToMany(mappedBy = "country", cascade = { CascadeType.REMOVE }, fetch = FetchType.EAGER)
	private Set<City> cities;

	public Country() {
	}

	public Country(String name, String code) {
		super();
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
