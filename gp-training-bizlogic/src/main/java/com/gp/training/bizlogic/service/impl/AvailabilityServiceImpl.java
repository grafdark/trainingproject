package com.gp.training.bizlogic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.dao.AvailabilityDao;
import com.gp.training.bizlogic.params.AvailSearchParams;
import com.gp.training.bizlogic.service.AvailabilityService;

public class AvailabilityServiceImpl implements AvailabilityService {

	@Autowired
	private AvailabilityDao availabilityDao;

	@Override
	public List<OfferDTO> findOffers(AvailSearchParams params) {
		return availabilityDao.findOffers(params);
	}

}
