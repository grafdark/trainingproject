package com.gp.training.bizlogic.service;

import com.gp.training.bizlogic.domain.Country;

import java.util.List;

public interface CountryService {
	Country takeCountryInfo(Long id);

	void addCountry(Country country);

	void deleteCountry(Long id);

	List<Country> takeCountries();
}
