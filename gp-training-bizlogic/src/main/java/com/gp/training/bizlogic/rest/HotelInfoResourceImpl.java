package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;
import com.gp.training.bizlogic.api.recource.HotelInfoRecource;
import com.gp.training.bizlogic.dao.HotelInfoDao;
import com.gp.training.bizlogic.domain.HotelInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HotelInfoResourceImpl implements HotelInfoRecource {

	@Autowired
	HotelInfoDao hotelInfoDao;

	@Override
	public HotelInfoDTO getHotelInfo(long hotelId) {
		HotelInfo hotelInfo = hotelInfoDao.getHotelInfo(Long.valueOf(hotelId));
		return convertHotelInfo2DTO(hotelInfo);
	}

	private HotelInfoDTO convertHotelInfo2DTO(HotelInfo hotelInfo) {
		HotelInfoDTO dto = new HotelInfoDTO();
		if (hotelInfo != null) {
			dto.setId(hotelInfo.getId());
			dto.setName(hotelInfo.getName());
			dto.setCode(hotelInfo.getCode());
			dto.setDescription(hotelInfo.getDescription());
		}
		return dto;
	}
}
