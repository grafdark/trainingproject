package com.gp.training.bizlogic.dao.impl;

import com.gp.training.bizlogic.dao.CountryDao;
import com.gp.training.bizlogic.domain.Country;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CountryDaoImpl implements CountryDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Country takeCountryInfo(Long id) {
		Country country = entityManager.find(Country.class, id);
		return country;
	}

	@Override
	public void addCountry(Country country) {
		entityManager.persist(country);
	}

	@Override
	public void deleteCountry(Long id) {
		Country country = entityManager.find(Country.class, id);
		if (null != country) {
			entityManager.remove(country);
		}
	}

	@Override
	public List<Country> takeCountries() {
		return entityManager.createQuery("from Country", Country.class).getResultList();
	}

}
