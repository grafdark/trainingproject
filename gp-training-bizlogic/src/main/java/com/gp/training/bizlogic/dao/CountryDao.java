package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Country;

import java.util.List;

public interface CountryDao {

	Country takeCountryInfo(Long id);
	
	void addCountry(Country country);

	void deleteCountry(Long id);

	List<Country> takeCountries();
}
