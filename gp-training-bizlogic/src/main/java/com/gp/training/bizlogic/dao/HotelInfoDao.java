package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;
import com.gp.training.bizlogic.domain.HotelInfo;

import java.util.List;

public interface HotelInfoDao {

	public HotelInfo getHotelInfo(Long id);

	void addHotel(HotelInfo hotelInfo);

	void deleteHotel(Long id);

	List<HotelInfo> takeHotels();

}
