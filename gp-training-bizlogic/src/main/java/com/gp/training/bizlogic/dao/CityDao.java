package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.City;

import java.util.List;

public interface CityDao extends GenericDao<City, Long> {
	
//	City takeCityInfo(Long id);
//
//	void addCity(City city);
//
//	void deleteCity(Long id);
//
//	List<City> takeCities();
}
