package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.MealType;

import java.util.List;

public interface MealTypeDao {
	MealType takeMealType(Long id);

	void addMealType(MealType mealType);

	void deleteMealType(Long id);

	List<MealType> takeMealTypes();
}
