package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;
import com.gp.training.bizlogic.api.recource.DictionaryRecource;
import com.gp.training.bizlogic.dao.MealTypeDao;
import com.gp.training.bizlogic.dao.RoomTypeDao;
import com.gp.training.bizlogic.domain.City;
import com.gp.training.bizlogic.domain.Country;
import com.gp.training.bizlogic.domain.MealType;
import com.gp.training.bizlogic.domain.RoomType;

@Component
public class DictionaryResource implements DictionaryRecource {

	@Autowired
	MealTypeDao mealTypeDao;
	@Autowired
	RoomTypeDao roomTypeDao;

	@Override
	public MealTypeDTO getMealType(long mealTypeId) {
		MealType mealType = mealTypeDao.takeMealType(Long.valueOf(mealTypeId));
		return convertMealType2DTO(mealType);
	}

	@Override
	public List<MealTypeDTO> takeMealTypes() {
		List<MealType> mealTypes = mealTypeDao.takeMealTypes();
		List<MealTypeDTO> mealTypeDtos = new ArrayList<>(mealTypes.size());
		for (MealType mealType : mealTypes) {
			MealTypeDTO dto = convertMealType2DTO(mealType);
			mealTypeDtos.add(dto);
		}
		return mealTypeDtos;
	}

	@Override
	public RoomTypeDTO getRoomType(long roomTypeId) {
		RoomType roomType = roomTypeDao.takeRoomType(Long.valueOf(roomTypeId));
		return convertRoomType2DTO(roomType);
	}

	@Override
	public List<RoomTypeDTO> takeRoomTypes() {
		List<RoomType> roomTypes = roomTypeDao.takeRoomTypes();
		List<RoomTypeDTO> roomTypeDtos = new ArrayList<>(roomTypes.size());
		for (RoomType roomType : roomTypes) {
			RoomTypeDTO dto = convertRoomType2DTO(roomType);
			roomTypeDtos.add(dto);
		}
		return roomTypeDtos;
	}

	private MealTypeDTO convertMealType2DTO(MealType mealType) {
		MealTypeDTO dto = new MealTypeDTO();
		if (mealType != null) {
			dto.setId(mealType.getId());
			dto.setName(mealType.getName());
			dto.setCode(mealType.getCode());
		}
		return dto;
	}

	private RoomTypeDTO convertRoomType2DTO(RoomType roomType) {
		RoomTypeDTO dto = new RoomTypeDTO();
		if (roomType != null) {
			dto.setId(roomType.getId());
			dto.setName(roomType.getName());
			dto.setCode(roomType.getCode());
		}
		return dto;
	}

}
