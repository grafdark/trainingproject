package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.domain.MealType;
import com.gp.training.bizlogic.service.MealTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/mealType")
public class MealTypeResource {
	@Autowired
	private MealTypeService mealTypeService;

	@GET
	@Path("{mealTypeId}")
	@Produces({ "application/json" })
	public MealType getMealType(@PathParam("mealTypeId") long mealTypeId) {
		MealType mealType = mealTypeService.takeMealType(mealTypeId);
		return mealType;
	}

	@GET
	@Path("/mealTypes")
	@Produces({ "application/json" })
	public List<MealType> takeMealTypes() {
		return mealTypeService.takeMealTypes();
	}

	@POST
	@Path("addMealType")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addMealType(MealType mealType) {
		mealTypeService.addMealType(mealType);
	}
	
	@DELETE
	@Path("{mealTypeId}")
	public void deleteMealType(@PathParam("mealTypeId") long mealTypeId) {
		mealTypeService.deleteMealType(mealTypeId);
	}
}
