package com.gp.training.bizlogic.service;

import com.gp.training.bizlogic.domain.MealType;

import java.util.List;

public interface MealTypeService {
	MealType takeMealType(Long id);

	void addMealType(MealType mealType);

	void deleteMealType(Long id);

	List<MealType> takeMealTypes();
}
