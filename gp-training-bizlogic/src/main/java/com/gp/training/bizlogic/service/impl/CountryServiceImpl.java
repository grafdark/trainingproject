package com.gp.training.bizlogic.service.impl;

import com.gp.training.bizlogic.dao.CountryDao;
import com.gp.training.bizlogic.domain.Country;
import com.gp.training.bizlogic.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	CountryDao countryDao;

	@Override
	public Country takeCountryInfo(Long id) {
		return countryDao.takeCountryInfo(id);
	}

	@Override
	public void addCountry(Country country) {
		countryDao.addCountry(country);
	}

	@Override
	public void deleteCountry(Long id) {
		countryDao.deleteCountry(id);
	}

	@Override
	public List<Country> takeCountries() {
		return countryDao.takeCountries();
	}

}
