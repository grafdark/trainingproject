package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.recource.CountryRecource;
import com.gp.training.bizlogic.dao.CountryDao;
import com.gp.training.bizlogic.domain.Country;
import com.gp.training.bizlogic.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

@Component
public class CountryResource implements CountryRecource {

	@Autowired
	private CountryDao countryDao;

	@Override
	public CountryDTO getCountry(long countryId) {
		Country country = countryDao.takeCountryInfo(Long.valueOf(countryId));
		return convertCountry2DTO(country);
	}

	@Override
	public List<CountryDTO> getCountries() {
		List<Country> countries = countryDao.takeCountries();
		List<CountryDTO> countryDtos = new ArrayList<>(countries.size());
		for (Country country : countries) {
			CountryDTO dto = convertCountry2DTO(country);
			countryDtos.add(dto);
		}

		return countryDtos;
	}

	private CountryDTO convertCountry2DTO(Country country) {
		CountryDTO dto = new CountryDTO();
		if (country != null) {
			dto.setId(country.getId());
			dto.setName(country.getName());
			dto.setCode(country.getCode());
		}
		return dto;
	}
}
