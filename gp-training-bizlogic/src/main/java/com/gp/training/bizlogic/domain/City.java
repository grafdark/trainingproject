package com.gp.training.bizlogic.domain;

import javax.persistence.*;

@Entity
@Table(name = "city")
public class City extends AbstractEntity {

	@Column(name = "name", insertable = true, nullable = false, unique = true, updatable = true)
	private String name;
	@Column(name = "code", insertable = true, nullable = false, unique = true, updatable = true)
	private String code;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id")
	private Country country;

	public City() {
	}


	public City(String name, String code, Country country) {
		super();
		this.name = name;
		this.code = code;
		this.country = country;
	}


	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

}
