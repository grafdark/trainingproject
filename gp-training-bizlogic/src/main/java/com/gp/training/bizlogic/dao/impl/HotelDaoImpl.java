package com.gp.training.bizlogic.dao.impl;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;
import com.gp.training.bizlogic.dao.HotelInfoDao;
import com.gp.training.bizlogic.domain.HotelInfo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

public class HotelDaoImpl implements HotelInfoDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public HotelInfo getHotelInfo(Long id) {
		HotelInfo hotel = entityManager.find(HotelInfo.class, id);
		return hotel;
	}

	@Override
	public void addHotel(HotelInfo hotelInfo) {
		entityManager.persist(hotelInfo);
	}

	@Override
	public void deleteHotel(Long id) {
		HotelInfo hotelInfo = entityManager.find(HotelInfo.class, id);
		if (null != hotelInfo) {
			entityManager.remove(hotelInfo);
		}
	}

	@Override
	public List<HotelInfo> takeHotels() {
		return entityManager.createQuery("from HotelInfo", HotelInfo.class)
				.getResultList();
	}

}
