package com.gp.training.bizlogic.domain;

import javax.persistence.*;

@Entity
@Table(name = "room_type")
public class RoomType extends AbstractEntity {
	@Column
	private String name;
	@Column
	private String code;
	@Column
	private Long guestCount;

	public RoomType() {
	}

	public RoomType(String name, String code, Long guestCount) {
		this.name = name;
		this.code = code;
		this.guestCount = guestCount;
	}


	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public Long getGuestCount() {
		return guestCount;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setGuestCount(Long guestCount) {
		this.guestCount = guestCount;
	}

}
