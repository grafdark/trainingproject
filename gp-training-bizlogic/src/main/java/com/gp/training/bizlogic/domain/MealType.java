package com.gp.training.bizlogic.domain;

import javax.persistence.*;

@Entity
@Table(name = "meal_type")
public class MealType extends AbstractEntity {
	@Column
	private String name;
	@Column
	private String code;

	public MealType() {
	}

	public MealType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
