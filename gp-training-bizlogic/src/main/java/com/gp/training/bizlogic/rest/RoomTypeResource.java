package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.domain.RoomType;
import com.gp.training.bizlogic.service.RoomTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/roomType")
public class RoomTypeResource {

	@Autowired
	private RoomTypeService roomTypeService;

	@GET
	@Path("{roomTypeId}")
	@Produces({ "application/json" })
	public RoomType getRoomType(@PathParam("roomTypeId") long roomTypeId) {
		RoomType roomType = roomTypeService.takeRoomType(roomTypeId);
		return roomType;
	}

	@GET
	@Path("/roomTypes")
	@Produces({ "application/json" })
	public List<RoomType> takeRoomTypes() {
		return roomTypeService.takeRoomTypes();
	}

	@POST
	@Path("addRoomType")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addRoomType(RoomType roomType) {
		roomTypeService.addRoomType(roomType);
	}
	
	@DELETE
	@Path("{mealRoomId}")
	public void deleteRoomType(@PathParam("mealRoomId") long roomTypeId) {
		roomTypeService.deleteRoomType(roomTypeId);
	}

}
