package com.gp.training.bizlogic.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class ClientSender {
	private String baseUrl;
	private ResteasyWebTarget restEasyTarget;

	public ClientSender() {

	}

	public ClientSender(String baseUrl) {
		this.baseUrl = baseUrl;

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(baseUrl);
		restEasyTarget = (ResteasyWebTarget) target;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public ResteasyWebTarget getRestEasyTarget() {
		return restEasyTarget;
	}

}
