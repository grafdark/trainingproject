package com.gp.training.bizlogic.api.recource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CountryDTO;

@Path("/country")
public interface CountryRecource {
	
	@GET
	@Path("{countryId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CountryDTO getCountry(@PathParam("countryId") long countryId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryDTO> getCountries();

}
