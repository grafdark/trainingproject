package com.gp.training.bizlogic.api.recource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;

public interface DictionaryRecource {

	@GET
	@Path("mealType/{mealTypeId}")
	@Produces({ "application/json" })
	public MealTypeDTO getMealType(@PathParam("mealTypeId") long mealTypeId);
	
	
	@GET
	@Path("/mealTypes")
	@Produces({ "application/json" })
	public List<MealTypeDTO> takeMealTypes();
	
	@GET
	@Path("{roomTypeId}")
	@Produces({ "application/json" })
	public RoomTypeDTO getRoomType(@PathParam("roomTypeId") long roomTypeId);

	@GET
	@Path("/roomTypes")
	@Produces({ "application/json" })
	public List<RoomTypeDTO> takeRoomTypes();

}
