package org.jboss.errai.enterprise.client.jaxrs;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.URL;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.service.AvailabilityResource;
import com.gp.training.web.shared.service.DictionaryResource;
import java.util.List;
import org.jboss.errai.common.client.api.ErrorCallback;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.common.client.framework.ProxyProvider;
import org.jboss.errai.common.client.framework.RemoteServiceProxyFactory;

public class JaxrsProxyLoaderImpl implements JaxrsProxyLoader { public void loadProxies() {
    class com_gp_training_web_shared_service_DictionaryResourceImpl extends AbstractJaxrsProxy implements DictionaryResource {
      private RemoteCallback remoteCallback;
      private ErrorCallback errorCallback;
      public com_gp_training_web_shared_service_DictionaryResourceImpl() {

      }

      public RemoteCallback getRemoteCallback() {
        return remoteCallback;
      }

      public void setRemoteCallback(RemoteCallback callback) {
        remoteCallback = callback;
      }

      public ErrorCallback getErrorCallback() {
        return errorCallback;
      }

      public void setErrorCallback(ErrorCallback callback) {
        errorCallback = callback;
      }

      public List getCountries() {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("dictionary/country");
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, List.class, CountryProxy.class);
          }
        });
        return null;
      }
    }
    RemoteServiceProxyFactory.addRemoteProxy(DictionaryResource.class, new ProxyProvider() {
      public Object getProxy() {
        return new com_gp_training_web_shared_service_DictionaryResourceImpl();
      }
    });
    class com_gp_training_web_shared_service_AvailabilityResourceImpl extends AbstractJaxrsProxy implements AvailabilityResource {
      private RemoteCallback remoteCallback;
      private ErrorCallback errorCallback;
      public com_gp_training_web_shared_service_AvailabilityResourceImpl() {

      }

      public RemoteCallback getRemoteCallback() {
        return remoteCallback;
      }

      public void setRemoteCallback(RemoteCallback callback) {
        remoteCallback = callback;
      }

      public ErrorCallback getErrorCallback() {
        return errorCallback;
      }

      public void setErrorCallback(ErrorCallback callback) {
        errorCallback = callback;
      }

      public List getOffers(final long a0, final int a1, final String a2, final String a3) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("search").append("?").append("endDate").append("=").append(URL.encodeQueryString(a3 == null ? "" : a3)).append("&").append("guestCount").append("=").append(URL.encodeQueryString(new Integer(a1).toString())).append("&").append("cityId").append("=").append(URL.encodeQueryString(new Long(a0).toString())).append("&").append("startDate").append("=").append(URL.encodeQueryString(a2 == null ? "" : a2));
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(String response) {
            return MarshallingWrapper.fromJSON(response, List.class, OfferProxy.class);
          }
        });
        return null;
      }
    }
    RemoteServiceProxyFactory.addRemoteProxy(AvailabilityResource.class, new ProxyProvider() {
      public Object getProxy() {
        return new com_gp_training_web_shared_service_AvailabilityResourceImpl();
      }
    });
  }
}