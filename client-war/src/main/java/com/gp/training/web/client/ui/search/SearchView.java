package com.gp.training.web.client.ui.search;

import java.util.List;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.service.AvailabilityResource;

public class SearchView extends Composite {

	private static SearchViewUiBinder uiBinder = GWT.create(SearchViewUiBinder.class);

	@UiField
	protected Button searchBtn;
	@UiField
	protected Label guestsCountLabel;
	@UiField
	protected TextBox guestCount;
	
	interface SearchViewUiBinder extends UiBinder<Widget, SearchView> {
	}

	public SearchView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		
		searchBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				 
				String guestCountStr = guestCount.getValue();
				
				int targetGuestCount = 2;
				if (guestCountStr != null && guestCountStr.length() > 0) {
					targetGuestCount = Integer.valueOf(guestCountStr);
				}
				
				RestClient.create(AvailabilityResource.class, new RemoteCallback<List<OfferProxy>>() {
					@Override
					public void callback(List<OfferProxy> response) {
						
						
					}
				}).getOffers(1, targetGuestCount, "2016-02-02", "2016-02-05");
				
			}
		});
	}

}
