package com.gp.training.web.client.entrypoint;

import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.RootPanel;
import com.gp.training.web.client.common.DictionaryService;
import com.gp.training.web.client.ui.search.SearchView;

public class ClientAppEntryPoint implements EntryPoint {
	
	 private static final String HTTP = "http://";
	 private static final String HTTPS = "https://";
	
	@Override
	public void onModuleLoad() {
		
		RestClient.setApplicationRoot(getApplicationURL() + "rest");
		
		loadDictionaries();
		
		SearchView searchView = new SearchView();
		// Don't forget, this is DOM only; will not work with GWT widgets
		Document.get().getBody().appendChild(searchView.getElement());
		
		RootPanel.get().add(searchView);
	}
	
	public static String getApplicationURL() {
	       String s = GWT.getHostPageBaseURL();
	       String baseUrl = GWT.getModuleBaseURL();
	       String baseStatic = GWT.getModuleBaseForStaticFiles();

	       if (s == null || s.isEmpty()) return "";
	       if (s.startsWith(HTTP)) s = s.substring(HTTP.length());
	       else if (s.startsWith(HTTPS)) s = s.substring(HTTPS.length());
	       int p = s.indexOf('/');
	       if (p == -1) return ""; // something strange
	       s = s.substring(p).trim();
	       return s;
	    }
		
	private void loadDictionaries() {
		
		DictionaryService.getCountries(null);
	}
}
