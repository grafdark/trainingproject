package com.gp.training.web.client.common;

import java.util.ArrayList;
import java.util.List;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.service.DictionaryResource;

public class DictionaryService {
	
	public static List<CountryProxy> countries = new ArrayList<>();
	
	public static void getCountries(Callback<List<CountryProxy>, Void> originCallback) {
		
		if (countries.size() > 0) {
			originCallback.onSuccess(countries);
		} else {
			loadCountries(originCallback);
		}	
	}
	
	private static void loadCountries(final Callback<List<CountryProxy>, Void> originCallback) {
		RemoteCallback<List<CountryProxy>> callback = new RemoteCallback<List<CountryProxy>>() {
            @Override
            public void callback(List<CountryProxy> response) {
                if (response != null && response.size() > 0) {
                	countries.addAll(response);
                	if (originCallback != null) originCallback.onSuccess(response);                	
                }
            }
        };	
		RestClient.create(DictionaryResource.class, callback).getCountries();
	}
}
