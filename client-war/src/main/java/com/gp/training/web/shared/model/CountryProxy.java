package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class CountryProxy {
	
	private int id;
	private String name;
	
	public CountryProxy() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
