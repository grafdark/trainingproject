package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class OfferProxy {
	
	private int offerId;
	private double price;
	private HotelProxy hotel;
	private RoomTypeProxy room;
	
	public OfferProxy() {
		
	}
	
    public OfferProxy(@MapsTo("offerId") int offerId, @MapsTo("price") double price,
    		@MapsTo("hotel") HotelProxy hotel, @MapsTo("room") RoomTypeProxy room) {
		this.offerId = offerId;
		this.price = price;
		this.hotel = hotel;
		this.room = room;
	}
	
	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public HotelProxy getHotel() {
		return hotel;
	}

	public void setHotel(HotelProxy hotel) {
		this.hotel = hotel;
	}

	public RoomTypeProxy getRoom() {
		return room;
	}

	public void setRoom(RoomTypeProxy room) {
		this.room = room;
	}
	
}
